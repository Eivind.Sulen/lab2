package INF101.lab2;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge{

    ArrayList<FridgeItem> fridgeList;
    public Fridge() {
        fridgeList = new ArrayList<>();
    }

    @Override
    public int nItemsInFridge() {
        // TODO Auto-generated method stub
        return fridgeList.size();
    }

    @Override
    public int totalSize() {
        // TODO Auto-generated method stub
        return 20;
    }

    @Override
    public boolean placeIn(FridgeItem item) {
        // TODO Auto-generated method stub
        if ( nItemsInFridge() < totalSize() ){
            fridgeList.add(item);
            return true;
        }
        return false;
    }

    @Override
    public void takeOut(FridgeItem item) {
        // TODO Auto-generated method stub

            if (fridgeList.contains(item)){
                fridgeList.remove(item);
                return;
            }
            throw new NoSuchElementException();

    }

    @Override
    public void emptyFridge() {
        // TODO Auto-generated method stub
        
        fridgeList = new ArrayList<>();
    }

    @Override
    public List<FridgeItem> removeExpiredFood() {
        // TODO Auto-generated method stub
        ArrayList<FridgeItem> expiredFood = new ArrayList<>();

        for (int i=0; i < nItemsInFridge(); i++){
            FridgeItem item = fridgeList.get(i);
            if (item.hasExpired()){
                expiredFood.add(item);
            }
        }
        for (int i = 0; i < expiredFood.size(); i++) {

            takeOut(expiredFood.get(i));
            
        }

        return expiredFood;
    }


}
